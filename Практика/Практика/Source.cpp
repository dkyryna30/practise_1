/**
* @file transf.c
* @author ������ �. �., ��. 515�
* @date 24.06.2019
* @brief ��������� ������ 
*
* �������������� � ���������� ���������. ������������� Git.
**/

#include<stdio.h>
#include<locale.h>
#include<math.h>
#include<iostream>
#define SIZE 1024
#define SIZE_V 10

double a, b, c;
double A, B, C;
double ma, mb, mc;
double ha, hb, hc;
double xA, xB, xC, yA, yB, yC;
double S, p;

void read_param(char*);
char* read_value(double*, char*);
void three_sides();
void output_param();
void two_sides_and_angle();
bool triangle_exists();
void tree_heights();
void three_coordinates();
void print_menu();

bool flag_trg_or_param = true;
bool error_input = false;

int main()
{
int key, k = 0;
char param_str[SIZE] = {'\0'};
setlocale(LC_ALL, "rus");
print_menu();
while(1)
{ 
	a = 0, b = 0, c = 0;
	 A = 0, B = 0, C = 0;
	 ma = 0, mb = 0, mc = 0;
	 ha = 0, hb = 0, hc = 0;
	 xA = 0, xB = 0, xC = 0, yA = 0, yB = 0, yC = 0;
	 S = 0, p = 0;
	printf("������� ���������: \n");
	gets(param_str);
	
	read_param(param_str);
	if(error_input)
	{
		printf("������������ ���� ����������!!!\n");
		error_input = false;
		continue;
	}

	flag_trg_or_param = triangle_exists();

	if(!flag_trg_or_param)
	{
		printf("������� ��������� ��� ���!!!\n");
		continue;
	}

	if(a != 0 && b != 0 && c != 0)
	{
		three_sides();
		if(!flag_trg_or_param)
		{
			continue;
		}
			output_param();

			continue;
	    }
	else if(a != 0 && b != 0 && C != 0 || b != 0 && c != 0 && A != 0 || a != 0 && c != 0 && B != 0)
	{	
		two_sides_and_angle();
		if(!flag_trg_or_param)
		{
			continue;
		}
		output_param();

		continue;
	}
	else if(ha != 0 && hb != 0 && hc != 0)
	{
		tree_heights();
		output_param();

		continue;
	}
	else if(xA != 0 && xB != 0 && xC != 0 && yA != 0 && yB != 0 && yC != 0)
	{
		three_coordinates();
		output_param();
		
		continue;
	}
	else
	{
		printf("������������ ����������!!!\n");
	}
}

system("pause");
return 0;
}

void read_param(char* param_str)
{
	char* reader = param_str;
	while(*reader != '\0')
	{
		if(error_input)
		{
			return;
		}
		switch(*reader)
		{
		case 'a':
			reader = read_value(&a, reader);
			break;
		case 'b':
			reader = read_value(&b, reader);
			break;
		case 'c':
			reader = read_value(&c, reader);
			break;
		case 'A':
			reader = read_value(&A, reader);
			break;
		case 'B':
			reader = read_value(&B, reader);
			break;
		case 'C':
			reader = read_value(&C, reader);
			break;
		case 'm':
			reader++;
			switch(*reader)
			{
			case 'a':
				reader = read_value(&ma, reader);
				break;
			case 'b':
				reader = read_value(&mb, reader);
				break;
			case 'c':
				reader = read_value(&mc, reader);
				break;
			}
			break;
		case 'h':
			reader++;
			switch(*reader)
			{
			case 'a':
				reader = read_value(&ha, reader);
				break;
			case 'b':
				reader = read_value(&hb, reader);
				break;
			case 'c':
				reader = read_value(&hc, reader);
				break;
			}
			break;
		case 'x':
			reader++;
			switch(*reader)
			{
			case 'A':
				reader = read_value(&xA, reader);
				break;
			case 'B':
				reader = read_value(&xB, reader);
				break;
			case 'C':
				reader = read_value(&xC, reader);
				break;
			}
			break;
		case 'y':
			reader++;
			switch(*reader)
			{
			case 'A':
				reader = read_value(&yA, reader);
				break;
			case 'B':
				reader = read_value(&yB, reader);
				break;
			case 'C':
				reader = read_value(&yC, reader);
				break;
			}
			break;
		case 'S':
			reader = read_value(&S, reader);
			break;
		case 'p':
			reader = read_value(&p, reader);
			break;
		default:
			error_input = true;
		}
	}
}

char* read_value(double*boofer, char*reader)
{
	bool numeral = false;
	bool separator = false;
	char value_in_str[SIZE_V] = {'\0'};
	char* ptr_value = value_in_str;
	reader++;
	while(!((*reader >= 'a' && *reader <= 'z') || (*reader >= 'A' && *reader <= 'Z') || (*reader == '\0')))
	{
		if((*reader >= '0' && *reader <= '9') || (*reader == '-'))
		{
			numeral = true;
			*ptr_value = *reader;
			reader++;
			ptr_value++;
			continue;
		}
		if(*reader == '.' || *reader == ';' || *reader == ',' )
		{
			separator = true;
			*boofer = atoi(value_in_str);
		}
		reader++;
	}
	value_in_str[0] = '\0';
	if(!numeral || !separator)
	{
		error_input = true;
		return reader;
	}
	else
	{
		return reader;
	}
}

void three_sides()
{
	bool flag = false;
	if(a < b && a < c)
	{
		if(b < c )
		{
			if(a + b < c)
			{
				printf("����������� �� ����������!!!\n");
				flag_trg_or_param = false;
				return;
			}
			else
			{
				flag = true;
			}
		}
	}
	if(!flag)
	{
		if(b < a && b < c)
		{
			if(c < a)
			{
				if(b + c < a)
				{
					printf("����������� �� ����������!!!\n");
					return;
				}
				else
				{
					flag = true;
				}

			}
		}
	}
	if(!flag)
	{
		if(a + c < b)
			{
				printf("����������� �� ����������!!!\n");
				return;
			}
			else
			{
				flag = true;
			}
	}
	p = (a + b + c) / 2;
	S = sqrt(p * (p - a) + p * (p - b) + p * (p - c));
	ha = (2 * S) / a;
	hb = (2 * S) / b;
	hc = (2 * S) / c;
	A = acos((b * b + c * c - a * a) / (2 * b * c));
	B = acos((a * a + c * c - b * b) / (2 * a * c));
	A = A * 180 / 3.14;
    B = B * 180 / 3.14;
	C = 180 - (A + B);

	double k = 0;
	k = 1.0 / 2.0;
	ma = (double) k * sqrt(b * b + c * c + 2.0 * c * b * cos(A));
	mb = (double) k * sqrt(2.0 * a * a + 2.0 * c * c - b * b);
	mc = (double) k * sqrt(2.0 * b * b + 2.0 * a * a - c * c);
}

void two_sides_and_angle()
{
	if(a != 0 && b != 0 && C != 0)
	{
	c = sqrt(((b * b) + (a * a)) - (2 * b * a * cos(C)));
		three_sides();
	}
	else if(a != 0 && c != 0 && B != 0)
	{
		b = sqrt(c * c + a * a - 2 * c * a * cos(B));
		three_sides();
	}
	else if(b != 0 && c != 0 && A != 0)
	{
		a = sqrt(b * b + c * c - 2 * c * b * cos(A));
		three_sides();
	}
	else
	{
		printf("���� ����������!!!\n");
		flag_trg_or_param = false;
	}

}

bool triangle_exists()
{
	if(a < 0 || b < 0 || c < 0)
	{
		printf("������� �� ����� ���� �������������!!!\n");
		return false;
	}

	if((A > 180 || A < 0) || (B > 180 || B < 0) || (C > 180 || C < 0))
	{
		printf("���� �� ����� ���� ������ 180 ��������!!\n");
		return false;
	}

	if(ha < 0 || hb < 0 || hc < 0)
	{
		printf("������ �� ����� ���� �������������!!!\n");
		return false;
	}

	if(ma < 0 || mb < 0 || mc < 0)
	{
		printf("������� �� ����� ���� �������������!!!\n");
		return false;
	}

	if(S < 0)
		{
		printf("������� �� ����� ���� �������������!!!\n");
		return false;
	}

	if(p < 0)
		{
		printf("������������ �� ����� ���� �������������!!!\n");
		return false;
	}

	if((a + b == c || b + c == a || a + c == b ) && a != 0 && b != 0 && c != 0)
	{
		printf("����������� �� ����������!!!\n");
		return false;
	}

	
	return true;
}

void tree_heights()
{
S = (double) 1 / (sqrt((1 / ha + 1 / hb + 1 / hc) * (1 / ha + 1 / hb - 1 / hc) * (1 / ha - 1 / hb + 1 / hc) * (1 / hb + 1 / hc - 1 / ha)));
a = 2 * S / ha;
b = 2 * S / hb;
c = 2 * S / hc;
three_sides();
}

void three_coordinates()
{
	c = sqrt((xB - xA) * (xB - xA) + (yB - yA) * (yB - yA));
	b = sqrt((xC - xA) * (xC - xA) + (yC - yA) * (yC - yA));
	a = sqrt((xC - xB) * (xC - xB) + (yC - yB) * (yC - yB));
	tree_heights();
}

void output_param()
{
	printf("������� � = %g\n", a);
	printf("������� b = %g\n", b);
	printf("������� c = %g\n", c);
	printf("���� � = %g\n", A);
	printf("���� B = %g\n", B);
	printf("���� C = %g\n", C);
	printf(" ������ ha = %g\n", ha);
	printf(" ������ hb = %g\n", hb);
	printf(" ������ hc = %g\n", hc);
	printf(" ������� ma = %g\n", ma);
	printf(" ������� mb = %g\n", mb);
	printf(" ������� mc = %g\n", mc);
	if(xA != 0 && yA != 0 && xB != 0 && yB != 0 && xC != 0 && yC != 0)
	{
		printf(" ���������� ����� A: x = %g, y = %g\n", xA, yA);
		printf(" ���������� ����� B: x = %g, y = %g\n", xB, yB);
		printf(" ���������� ����� C: x = %g, y = %g\n", xC, yC);
	}
	else
	{
		printf("���������� �� ����� ���� ���������!!!\n");
	}
	printf(" ������� S = %g\n", S);
	printf(" ������������ p = %g\n", p);
}

void print_menu()
{
	printf("\t\t\t���������� �� ������������� ������������\n");
	printf("������� ���������������  ������ ����  ���������� ��� �� ������ ��������� ������. �� ���� �������� ���� � ����� �������\n������������ �, ������� � ������������ ���� �.\n");
	printf("ma - ��� ������, �������� �� ������� �, �������������� ���� ��� ������� mb � mc �������� �� ��������������� �������.\n");
	printf("hb - ��� ������, �������� �� ������� b, �������������� ���� ��� ������ ha � hc �������� �� ��������������� �������.\n");
	printf("�� � ������, ������� ���  ������������� �������� ������ � ������� ������������ ��������������� �������:\n");
	printf("����� �����(!) ���� ������ ������ ���� ������ �������.\n");
	printf("������� �� ����������� ���� �������� ������ ��� ����� ������ ������������ �� ���������� ��� ������� ������������\n��������� ������������ �� ��������� 3, 3 � 7.\n");
	printf("��������� ������� ��������, ����������� ������ � ��������������� ����������� ��� (�������� = ��������)\n");
	printf("�������� ���� �������� ������� � � ��������� 10, �� ��� � ���������� a = 10\n");
	printf("������ ��������� ������� ����� ������������  � ��������:\n");
    printf("������� a\n������� b\n������� c\n������������ p\n���� �\n���� B\n���� C\n������� ������������ S\n������ ha �� ������� a\n������ hb �� ������� b\n������ hc �� ������� c\n");
	printf("������� ma �� ������� a\n������� mb �� ������� b\n������� mc �� ������� c\n���������� ������ (xA,yA) (xB,yB) (xC,yC)\n");	
	printf("������ ����� ����������: � = 2; b = 5; C = 45;\n");
	printf("������ ���������� ��������� ������ ����������!!!\n");
}